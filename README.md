# Kstratum-Infra

This is the infra for the kaspad-stratum project it is intended to be a one stop shop for a Kaspa stratum mining pool.

## Contributions

We welcome all PRs or contributions to this project

```
kaspa:qzwrhsjnknjzrnw2daq7czsqlwzvp23fcj2j4zsajks75c4pjjxk687grpgkp
```

## Validation

* kstratumrust

```
/kstratum-infra [git::master *] [kubeconfig]
> ./1.62/lolMiner -a KASPA -p 144.202.24.181:6969 -u kaspa:XXXXXXXXXXXX
+---------------------------------------------------------+
|   _       _ __  __ _                   _    __   ____   |
|  | | ___ | |  \/  (_)_ __   ___ _ __  / |  / /_ |___ \  |
|  | |/ _ \| | |\/| | | '_ \ / _ \ '__| | | | '_ \  __) | |
|  | | (_) | | |  | | | | | |  __/ |    | |_| (_) |/ __/  |
|  |_|\___/|_|_|  |_|_|_| |_|\___|_|    |_(_)\___/|_____| |
|                                                         |
|              This software is for mining                |
|              Autolykos V2                               |
|              Ethash, Etchash                            |
|              Equihash 144/5, 192/7, 210/9               |
|              BeamHash III                               |
|              Flux (ZelHash)                             |
|              kHeavyhash (Kaspa)                         |
|              Blake3 (Alephium)                          |
|              Cuck(ar)oo 29                              |
|              Cuckaroo   30 CTX                          |
|              Cuckatoo   31/32                           |
|                                                         |
|                                                         |
|           Made by Lolliedieb, November 2022             |
+---------------------------------------------------------+

Setup Miner...
OpenCL driver detected.
Number of OpenCL supported GPUs: 1
Cuda driver detected.
Number of Cuda supported GPUs: 1
Device 0:
    Name:    NVIDIA GeForce RTX 2080
    Address: 1:0
    Vendor:  NVIDIA Corporation
    Drivers: Cuda, OpenCL
    Memory:  7959 MByte
    Active:  true (Selected Algorithm: HeavyHash (Kaspa))

Connecting to pool...
Connected to 144.202.24.181(144.202.24.181):6969  (TLS disabled)
New extra nonce received: 0001
New target received: 00000002ca32fffd (Diff 0.3584434052463621)
Start Mining...
Average speed (15s): 518.98 Mh/s
Average speed (15s): 509.40 Mh/s
Average speed (15s): 513.48 Mh/s
-----------------------------------------------
Statistics (12:24:22); Uptime: 0h 1m 0s
lolMiner 1.62, Nvidia 470.141.03
Mining: HeavyHash-Kaspa
Connected to: 144.202.24.181:6969

      Name      Speed  Pool  Shares   Best    Eff.  Power  CCLK  MCLK  Core  Junc   Mem  Fan
                 Mh/s  Mh/s     A/R  Share  Mh/s/W      W   MHz   MHz  Temp  Temp  Temp  Pct
GPU 0 RTX 2080 509.48  0.00     0/0    0.0   2.527  201.1  1800  6800    67    78    75   44
---------------------------
Total          509.48  0.00     0/0    0.0   2.527  201.1
-----------------------------------------------
Average speed (15s): 507.13 Mh/s

```
