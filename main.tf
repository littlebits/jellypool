locals {
  kubeconfig = yamldecode(base64decode(vultr_kubernetes.jellyCluster.kube_config))
}

provider "vultr" {
  api_key = var.VULTR_TOKEN
}

resource "vultr_kubernetes" "jellyCluster" {
  label   = "jellyCluster"
  version = "v1.24.8+1"
  region  = "atl"
  node_pools {
    node_quantity = 1
    plan          = "vc2-2c-4gb"
    label         = "jellycluster"
    auto_scaler   = true
    min_nodes     = 1
    max_nodes     = 2
  }
}

provider "kubernetes" {
  host                   = local.kubeconfig.clusters[0].cluster.server
  client_certificate     = base64decode(local.kubeconfig.users[0].user.client-certificate-data)
  client_key             = base64decode(local.kubeconfig.users[0].user.client-key-data)
  cluster_ca_certificate = base64decode(local.kubeconfig.clusters[0].cluster.certificate-authority-data)
}

resource "kubernetes_secret" "kaspad-straturm-wallet" {
  metadata {
    name = "kaspad-stratum-${var.instance}"
    labels = {
      app      = "stratum"
      instance = "${var.instance}"
    }
  }
  data = {
    wallet = "${var.wallet}"
  }
}

resource "kubernetes_config_map" "kaspadConfig" {
  metadata {
    name = "kaspad-config-${var.instance}"
    labels = {
      app      = "kaspad"
      instance = "${var.instance}"
    }
  }
  data = {
    "kaspad.conf" = <<EOT
[Application Options]
listen=
rpclisten=
	EOT
  }
}

resource "kubernetes_persistent_volume_claim" "kaspad" {
  metadata {
    name = "kaspad-pvc"
    labels = {
      app      = "kaspapool"
      instance = "${var.instance}"
    }
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    resources {
      requests = {
        storage = "40Gi"
      }
    }
  }
}

resource "kubernetes_secret" "regcred" {
  metadata {
    name = "regcred"
    labels = {
      app = "kaspadpool"
    }
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = "{ \"auths\" : { \"registry.gitlab.com\" : { \"username\" : \"${var.gitlab_user}\", \"password\" : \"${var.gitlab_pass}\" } } }"
  }
}

resource "kubernetes_deployment" "kaspad" {
  metadata {
    name = "kaspad-${var.instance}"
    labels = {
      app      = "kaspad"
      instance = "${var.instance}"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app      = "kaspad"
        instance = "${var.instance}"
      }
    }
    template {
      metadata {
        labels = {
          app      = "kaspad"
          instance = "${var.instance}"
        }
      }
      spec {
        image_pull_secrets {
          name = "regcred"
        }
        init_container {
          name    = "permissions-init"
          image   = "busybox:latest"
          command = ["sh", "-c", "chown 51591 -R /app/data/"]
          volume_mount {
            mount_path = "/app/data"
            name       = "kaspadata"
          }
        }
        container {
          image             = "${var.registry}/kaspad:${var.revision}"
          image_pull_policy = "IfNotPresent"
          name              = "kaspad"
          port {
            protocol       = "TCP"
            name           = "rpc"
            container_port = "16110"
          }
          port {
            protocol       = "TCP"
            name           = "listen"
            container_port = "16111"
          }
          volume_mount {
            mount_path = "/app/data"
            name       = "kaspadata"
          }
        }
        volume {
          name = "kaspadata"
          persistent_volume_claim {
            claim_name = "kaspad-pvc"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_secret.regcred,
    kubernetes_persistent_volume_claim.kaspad
  ]
}

resource "kubernetes_deployment" "kaspa-stratum" {
  metadata {
    name = "kstratumrust-${var.instance}"
    labels = {
      app      = "stratum"
      instance = "${var.instance}"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app      = "stratum"
        instance = "${var.instance}"
      }
    }
    template {
      metadata {
        labels = {
          app      = "stratum"
          instance = "${var.instance}"
        }
      }
      spec {
        image_pull_secrets {
          name = "regcred"
        }
        container {
          image             = "${var.registry}/kstratumrust:${var.revision}"
          image_pull_policy = "IfNotPresent"
          name              = "stratum"
          env {
            name = "WALLET"
            value_from {
              secret_key_ref {
                name = "kaspad-stratum-${var.instance}"
                key  = "wallet"
              }
            }
          }
          args = [
            "-r",
            "kaspad-svc:16110",
            "-m",
            "$(WALLET)",
            "-s",
            "0.0.0.0:16112",
          ]
          port {
            protocol       = "TCP"
            name           = "stratum"
            container_port = "16112"
          }
        }
      }
    }
  }
  depends_on = [
    kubernetes_secret.regcred,
    kubernetes_secret.kaspad-straturm-wallet,
    kubernetes_deployment.kaspad,
  ]
}

resource "kubernetes_service" "kaspad" {
  metadata {
    name = "kaspad-svc"
    labels = {
      app      = "kaspad"
      instance = "${var.instance}"
    }
  }
  spec {
    selector = {
      app      = "kaspad"
      instance = "${var.instance}"
    }
    port {
      name        = "rpc"
      port        = 16110
      target_port = 16110
    }
    port {
      name        = "listen"
      port        = 16111
      target_port = 16111
    }
  }
}

resource "kubernetes_service" "kaspa-stratum" {
  metadata {
    name = "stratum-svc"
    labels = {
      app      = "stratum"
      instance = "${var.instance}"
    }
  }
  spec {
    selector = {
      app      = "stratum"
      instance = "${var.instance}"
    }
    port {
      name        = "stratum"
      port        = 16112
      target_port = 16112
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

provider "helm" {
  kubernetes {
    host                   = local.kubeconfig.clusters[0].cluster.server
    client_certificate     = base64decode(local.kubeconfig.users[0].user.client-certificate-data)
    client_key             = base64decode(local.kubeconfig.users[0].user.client-key-data)
    cluster_ca_certificate = base64decode(local.kubeconfig.clusters[0].cluster.certificate-authority-data)
  }
}

resource "helm_release" "metrics-server" {
  name       = "metrics-server"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"
  namespace  = "monitoring"

  set {
    name  = "startupProbe.initialDelaySeconds"
    value = "120s"
  }

  set {
    name  = "apiService.create"
    value = "true"
  }

  set {
    name  = "extraArgs[0]"
    value = "--kubelet-preferred-address-types=InternalIP"
  }

  set {
    name  = "extraArgs[1]"
    value = "--kubelet-insecure-tls"
  }

  depends_on = [
    kubernetes_namespace.monitoring
  ]
}

